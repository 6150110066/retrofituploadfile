package com.example.retrofituploadfile;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

public interface FileApi {
    public static String BASE_URL = "http://10.0.2.2/retrofit/";
    @Multipart
    @POST("upload.php")
    Call<Respond> uploadImage(@Part MultipartBody.Part file, @Part("desc")
            RequestBody name );
}

